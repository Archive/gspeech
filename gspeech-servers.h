#ifndef G_SPEECH_SERVER__H_
#define G_SPEECH_SERVER__H_

#include <glib.h>

typedef struct _SpeechServer SpeechServer;

struct _SpeechServer {
	gchar    *name;
	gchar    *description;
	gpointer  data;
	int       speed;
	int       pitch;

	int    (*create)     (SpeechServer* server);
	int    (*init)       (SpeechServer* server);
	void   (*destroy)    (SpeechServer* server);	
	int    (*parse)      (SpeechServer* server, GScanner *scanner);
	
	void   (*say)       (SpeechServer* server, gchar *string);
	void   (*stop)       (SpeechServer* server);
	GList* (*languages)  (SpeechServer* server);
	GList* (*voices)     (SpeechServer* server, gchar *lang);
	void   (*set_lang)   (SpeechServer* server, gchar *lang);
	void   (*set_voice)  (SpeechServer* server, gchar *voice);
	void   (*set_speed)  (SpeechServer* server, gint speed);
	void   (*set_pitch)  (SpeechServer* server, gint pitch);
	void   (*set_mode)   (SpeechServer* server, gchar *mode);
	/* more funcs */
};

SpeechServer* gspeech_server_new           (char *name);
void          gspeech_server_init          (SpeechServer* server);
void          gspeech_server_parse         (SpeechServer* server, char *text, GScanner* scanner);
void          gspeech_server_stop          (SpeechServer* server);
void          gspeech_server_destroy       (SpeechServer* server);
GList*        gspeech_server_get_languages (SpeechServer* server);
GList*        gspeech_server_get_voices    (SpeechServer* server, char *lang);
void          gspeech_server_say           (SpeechServer* server, char *text);
void          gspeech_server_set_language  (SpeechServer* server, char *lang);
void          gspeech_server_set_voice     (SpeechServer* server, char *voice);
void          gspeech_server_set_mode      (SpeechServer* server, char *mode);
void          gspeech_server_set_speed     (SpeechServer* server, gint speed);
void          gspeech_server_change_speed  (SpeechServer* server, gint diff);
void          gspeech_server_set_pitch     (SpeechServer* server, gint pitch);
void          gspeech_server_change_pitch  (SpeechServer* server, gint diff);

#endif

