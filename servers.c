#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <signal.h>
#include "gspeech-servers.h"

typedef struct _festival_info festival_info;
typedef struct _command_info command_info;

enum {
	P_BEGIN,
	P_ERROR,
	P_EXEC,
	P_QUOTE,
	P_RESTART,
	P_PIPE,
	P_PORT
};

struct _festival_info {
	int port;
	int pipe;
	int use_esd;
	gchar *host;
	gchar *command;
	
	int fd;
	pid_t pid;
};

struct _command_info {
	int quote;
	int pipe;
	int wait;
	gchar *command;

	int fd[2];
	pid_t pid;
};

static int
festival_create (SpeechServer *server) {
	festival_info* info;

	if ( server->data )
		return 0;

	info = g_new0(festival_info, 1);
	info->pid = -1;
	info->port = 1314;

	server->data = info;
	return 0;
}

static int
festival_init (SpeechServer *server) {
	festival_info* info;
	struct sockaddr_in name;
	int tries = 2;
	char *cmd = "(audio_mode 'async)\n";

	info = (festival_info*)server->data;

	info->fd = socket(PF_INET, SOCK_STREAM, 0);
	if (info->fd < 0) {
		perror("socket");
		return 1;
	}
	name.sin_family = AF_INET;
	name.sin_port = htons (info->port);
	name.sin_addr.s_addr = htonl (INADDR_ANY);
	while (connect (info->fd, (struct sockaddr *) &name, sizeof (name)) < 0) {
		if (!tries--) {
			perror("connect");
			close(info->fd);
			return 1;
		}
		system(info->use_esd?"esddsp festival --server >/dev/null &":"festival --server >/dev/null &");
		sleep(2);
	}
	/*write(info->fd, cmd, strlen(cmd));*/
	return 0;
}

static void
festival_destroy (SpeechServer *server) {
	festival_info* info;
	int status;

	info = (festival_info*)server->data;

	if ( info->pid >= 0 ) {
		kill(info->pid, SIGTERM);
		waitpid(info->pid, &status, WNOHANG);
	} else if ( info->fd >= 0 ) {
		close(info->fd);
	}
	if (info->host)
		g_free(info->host);
		 
	g_free(info);
	server->data = NULL;
}

static int
festival_parse(SpeechServer *server, GScanner *scanner) {
	GTokenValue val;
	int tok;
	int state = P_BEGIN;
	festival_info* info;

	info = (festival_info*)server->data;
	tok=g_scanner_get_next_token(scanner);
	if ( tok != G_TOKEN_LEFT_CURLY )
		return 0;

	while ((tok=g_scanner_get_next_token(scanner)) != G_TOKEN_EOF ) {
		if (tok == G_TOKEN_RIGHT_CURLY)
			break;
		val = g_scanner_cur_value(scanner);
		switch(state) {
		case P_ERROR:
			if ( tok == ';' )
				state = P_BEGIN;
			break;
		case P_PORT:
			if (tok != G_TOKEN_INT) {
				state = P_ERROR;
				g_warning("Need an integer as a port");
				break;
			}
			info->port = val.v_int;
			tok=g_scanner_get_next_token(scanner);
			if (tok != ';') {
				state = P_ERROR;
				g_warning("Need an ; after port");
				break;
			}
			state = P_BEGIN;
			break;
		case P_EXEC:
			if (tok != G_TOKEN_STRING) {
				state = P_ERROR;
				g_warning("Need an string as a command");
				break;
			}
			if (info->command)
				g_free(info->command);
			info->command = g_strdup(val.v_string);
			tok=g_scanner_get_next_token(scanner);
			if (tok != ';') {
				state = P_ERROR;
				g_warning("Need an ; after quote");
				break;
			}
			state = P_BEGIN;
			break;
		case P_BEGIN:
			if (tok != G_TOKEN_IDENTIFIER) {
				state = P_ERROR;
				g_warning("Need an identifier in server command (%d-%c)", tok, tok);
				break;
			}
			if (!strcmp("exec", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != '=') {
					state = P_ERROR;
					g_warning("Need an = after exec");
					break;
				}
				state = P_EXEC;
				break;
			} else if (!strcmp("port", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != '=') {
					state = P_ERROR;
					g_warning("Need an = after port");
					break;
				}
				state = P_PORT;
				break;
			} else if (!strcmp("pipe", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != ';') {
					state = P_ERROR;
					g_warning("Need an ; after pipe");
					break;
				}
				info->pipe = 1;
				break;
			} else if (!strcmp("esd", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != ';') {
					state = P_ERROR;
					g_warning("Need an ; after esd");
					break;
				}
				info->use_esd = 1;
				break;
			}
		default:
			g_warning("Unknown state %d in festival", state);
			state = P_ERROR;
			break;
		}
	}
	return 0;
}

static void
festival_say(SpeechServer *server, gchar *text) {
	festival_info* info;
	gchar *quoted;
	gchar *p;

	info = (festival_info*)server->data;
	
	quoted = g_malloc(64+strlen(text)*2);

	strcpy(quoted, "(SayText \"");
	p = quoted+10;
	while (*text) {
		if ( *text == '\\' || *text == '"' )
			*p = '\\';
		*p++ = *text++;
	}
	*p++ = '"';
	*p++ = ')';
	*p = 0;

	write(info->fd, quoted, strlen(quoted));
	g_free(quoted);
}

static void
festival_stop (SpeechServer *server) {
}

static GList*
festival_languages (SpeechServer *server) {
	GList * lang = NULL;
	lang = g_list_append(lang, g_strdup("english"));
	lang = g_list_append(lang, g_strdup("spanish"));
	/*lang = g_list_append(lang, g_strdup("welsh"));*/
	return lang;
}

static GList*
festival_voices (SpeechServer *server, gchar* lang) {
	char* e_voices[] = {
		"rab_diphone", "don_diphone", "ken_diphone", NULL
	};
	char* s_voices[] = {
		"spanish_el", NULL
	};
	int i;
	char ** av = NULL;
	GList *result = NULL;

	if (!strcmp(lang, "english"))
		av = e_voices;
	else if (!strcmp(lang, "spanish"))
		av = s_voices;
	if (!av)
		return NULL;
	for (i=0; av[i]; ++i) {
		result = g_list_append(result, g_strdup(av[i]));
	}
	return result;
}

static void
festival_set_lang(SpeechServer *server, gchar *lang) {
	festival_info* info;
	gchar *cmd;

	info = (festival_info*)server->data;
	
	cmd = g_strdup_printf("(select_language '%s)\n", lang);
	write(info->fd, cmd, strlen(cmd));
	g_free(cmd);
}

static void
festival_set_voice(SpeechServer *server, gchar *voice) {
	festival_info* info;
	gchar *cmd;

	info = (festival_info*)server->data;
	
	cmd = g_strdup_printf("(voice_%s)\n", voice);
	write(info->fd, cmd, strlen(cmd));
	g_free(cmd);
}

static void
festival_set_speed (SpeechServer *server, gint speed) {
	festival_info* info;
	gchar *cmd;
	double nspeed; /* 0.5 - 4.5  for festival */

	nspeed = 4.5 - speed/25.0;
	info = (festival_info*)server->data;
	
	cmd = g_strdup_printf("(Parameter.set 'Duration_Stretch %.2f)\n", nspeed);
	write(info->fd, cmd, strlen(cmd));
	g_free(cmd);
}

static SpeechServer
festival_server = {
	"festival",
	"Festival speech server.",
	NULL,
	80,
	50,
	festival_create,
	festival_init,
	festival_destroy,
	festival_parse,
	festival_say,
	festival_stop,
	festival_languages,
	festival_voices,
	festival_set_lang,
	festival_set_voice,
	festival_set_speed
};

/* The command speech server */
static int
command_create (SpeechServer *server) {
	command_info* info;

	if ( server->data )
		return 0;

	info = g_new0(command_info, 1);
	info->pid = -1;

	server->data = info;
	return 0;
}

static int
command_init (SpeechServer *server) {
	return 0;
}

static void
command_destroy (SpeechServer *server) {
	command_info* info;
	int status;

	info = (command_info*)server->data;

	if ( info->pid >= 0 ) {
		kill(info->pid, SIGTERM);
		waitpid(info->pid, &status, WNOHANG);
	}
	if (info->command)
		g_free(info->command);
		 
	g_free(info);
	server->data = NULL;
}

static int
command_parse(SpeechServer *server, GScanner *scanner) {
	GTokenValue val;
	int tok;
	int state = P_BEGIN;
	command_info* info;

	info = (command_info*)server->data;
	tok=g_scanner_get_next_token(scanner);
	if ( tok != G_TOKEN_LEFT_CURLY )
		return 0;

	while ((tok=g_scanner_get_next_token(scanner)) != G_TOKEN_EOF ) {
		if (tok == G_TOKEN_RIGHT_CURLY)
			break;
		val = g_scanner_cur_value(scanner);
		switch(state) {
		case P_EXEC:
			if (tok != G_TOKEN_STRING) {
				state = P_ERROR;
				g_warning("Need an string as a command");
				break;
			}
			if (info->command)
				g_free(info->command);
			info->command = g_strdup(val.v_string);
			tok=g_scanner_get_next_token(scanner);
			if (tok != ';') {
				state = P_ERROR;
				g_warning("Need an ; after quote");
				break;
			}
			state = P_BEGIN;
			break;
		case P_BEGIN:
			if (tok != G_TOKEN_IDENTIFIER) {
				state = P_ERROR;
				g_warning("Need an identifier in server command (%d-%c)", tok, tok);
				break;
			}
			if (!strcmp("exec", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != '=') {
					state = P_ERROR;
					g_warning("Need an = after exec");
					break;
				}
				state = P_EXEC;
				break;
			} else if (!strcmp("quote", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != ';') {
					state = P_ERROR;
					g_warning("Need an ; after quote");
					break;
				}
				info->quote = 1;
				break;
			} else if (!strcmp("wait", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != ';') {
					state = P_ERROR;
					g_warning("Need an ; after wait");
					break;
				}
				info->wait = 1;
				break;
			} else if (!strcmp("pipe", val.v_identifier)) {
				tok=g_scanner_get_next_token(scanner);
				if (tok != ';') {
					state = P_ERROR;
					g_warning("Need an ; after pipe");
					break;
				}
				info->pipe = 1;
				break;
			}
		default:
			g_warning("Unknown state %d in command", state);
			state = P_ERROR;
			break;
		}
	}
	return 0;
}

static void
command_say(SpeechServer *server, gchar *text) {
	command_info* info;
	gchar *quoted;
	gchar *p;
	int res;

	info = (command_info*)server->data;

	p = g_strdup_printf(info->command, text);
	res = system(p);
	/*g_message("Exec (%d): %s", res, p);*/
}

static void
command_stop (SpeechServer *server) {
	command_info* info;

	info = (command_info*)server->data;
	
	kill(info->pid, SIGTERM);
}

static GList*
command_languages (SpeechServer *server) {
	return g_list_append(NULL, g_strdup("english"));
}

static GList*
command_voices (SpeechServer *server, gchar* lang) {
	return g_list_append(NULL, g_strdup("male"));
}

static void
command_set_lang(SpeechServer *server, gchar *lang) {
}

static void
command_set_voice(SpeechServer *server, gchar *voice) {
}

static void
command_set_speed (SpeechServer *server, gint speed) {
}

static SpeechServer
command_server = {
	"command",
	"A command-line based speech server",
	NULL,
	80,
	50,
	command_create,
	command_init,
	command_destroy,
	command_parse,
	command_say,
	command_stop,
	command_languages,
	command_voices,
	command_set_lang,
	command_set_voice,
	command_set_speed
};

static SpeechServer*
gspeech_default_servers[] = {
	&festival_server,
	&command_server,
	NULL
};

static GList* server_list = NULL;

SpeechServer* 
gspeech_server_new (char *name) {
	SpeechServer* server, *newserver;
	GList* list;
	int i;

	g_return_if_fail(server != NULL);

	server = NULL;
	for (list=server_list; list; list=list->next) {
		server = (SpeechServer*)list->data;
		if (!strcmp(name, server->name))
			break;
		server = NULL;
	}
	if (!server) {
		for (i=0; gspeech_default_servers[i]; ++i) {
			if (!strcmp(name, gspeech_default_servers[i]->name)) {
				server = gspeech_default_servers[i];
				break;
			}
		}
	}
	if (!server)
		return NULL;
	newserver = g_new0(SpeechServer, 1);
	*newserver = *server;
	if (newserver->create) {
		newserver->create(newserver);
	}
	gspeech_server_init(newserver);
	return newserver;
}

void 
gspeech_server_say (SpeechServer* server, char *text) {
	g_return_if_fail(server != NULL);
	
	server->say(server, text);
}

void 
gspeech_server_stop (SpeechServer* server) {
	g_return_if_fail(server != NULL);
	
	server->stop(server);
}

void 
gspeech_server_init (SpeechServer* server) {
	g_return_if_fail(server != NULL);
	
	server->init(server);
}

void 
gspeech_server_parse (SpeechServer* server, char* text, GScanner *scanner) {
	g_return_if_fail(server != NULL);
	
	if (text) {
		scanner = g_scanner_new(NULL);
		g_scanner_input_text (scanner, text, strlen(text));
	}
	server->parse(server, scanner);
	if (text)
		g_scanner_destroy(scanner);
}

void 
gspeech_server_destroy (SpeechServer* server) {
	g_return_if_fail(server != NULL);
	
	server->destroy(server);
	g_free(server);
}

GList* 
gspeech_server_get_languages (SpeechServer* server) {
	g_return_val_if_fail(server != NULL, NULL);

	return server->languages(server);
}

GList* 
gspeech_server_get_voices (SpeechServer* server, gchar* lang) {
	g_return_val_if_fail(server != NULL, NULL);

	return server->voices(server, lang);
}

void gspeech_server_set_language  (SpeechServer* server, char *lang) {
	g_return_if_fail(server != NULL);
	
	server->set_lang(server, lang);
}

void 
gspeech_server_set_voice (SpeechServer* server, char *voice) {
	g_return_if_fail(server != NULL);
	
	server->set_voice(server, voice);
}

void 
gspeech_server_set_mode (SpeechServer* server, char *mode) {
	g_return_if_fail(server != NULL);
	
	server->set_mode(server, mode);
}

void 
gspeech_server_set_speed (SpeechServer* server, gint speed) {
	g_return_if_fail(server != NULL);
	
	if (speed < 0)
		speed = 0;
	if (speed > 100)
		speed = 100;
	server->speed = speed;
	server->set_speed(server, speed);
}

void 
gspeech_server_change_speed (SpeechServer* server, gint diff) {
	int newval;
	
	g_return_if_fail(server != NULL);

	newval = server->speed + diff;
	gspeech_server_set_speed(server, newval);
}

void 
gspeech_server_set_pitch (SpeechServer* server, gint pitch) {
	g_return_if_fail(server != NULL);
	
	server->pitch = pitch;
	server->set_pitch(server, pitch);
}

void 
gspeech_server_change_pitch (SpeechServer* server, gint diff) {
	int newval;
	
	g_return_if_fail(server != NULL);

	newval = server->pitch + diff;
	gspeech_server_set_pitch(server, newval);
}

/* FIXME: add function to add servers */
