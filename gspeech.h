#ifndef G_SPEECH_H_
#define G_SPEECH_H_

#include <gtk/gtkwidget.h>
/*#include "gspeech-servers.h"*/

typedef gchar* (*SpeechFunc)(GtkObject*, guint sig, guint n, GtkArg *args, gpointer data);
typedef gint   (*SpeechKey) (GtkWidget*, GdkEventKey* e, gpointer data);

typedef struct _SpeechIEntry SpeechIEntry;
typedef struct _SpeechKEntry SpeechKEntry;

struct _SpeechIEntry {
	gchar      *name;
	SpeechFunc  func;
	gchar      *description;
	gpointer    data;
};

struct _SpeechKEntry {
	gchar      *name;
	SpeechKey  func;
	gchar      *description;
	gpointer    data;
};

extern SpeechIEntry gspeech_default_internals[];
extern SpeechKEntry gspeech_default_snoopers[];

gchar* gspeech_get_config       (gchar* key);
int    gspeech_get_config_int   (gchar* key, int def);
int    gspeech_get_config_accel (gchar* key, guint *k, guint *mod);

#endif

