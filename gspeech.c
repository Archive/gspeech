#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkaccelgroup.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkcontainer.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkvscale.h>
#include <gtk/gtk.h>
#include <gmodule.h>
#include <esd.h>

#include "gspeech.h"
#include "gspeech-servers.h"

#ifdef USE_PERL
#include <EXTERN.h>
#include <perl.h>
#endif

/* use gtk_accelerator parse() */

typedef struct _SpeechHook SpeechHook;
typedef struct _SpeechData SpeechData;
typedef struct _SpeechSnoop SpeechSnoop;
typedef struct _SpeechSet SpeechSet;
typedef struct _SpeechSound SpeechSound;

struct _SpeechData {
	guint type;
	union {
		guint number;
		gchar* text;
		SpeechFunc func;
	} u;
};

struct _SpeechHook {
	gchar* name;
	gchar* widget;
	gchar* signal;
	guint signal_id;
	guint emission_id;
	guint active;
	GtkType type;
	GList *actions;
};

struct _SpeechSnoop {
	gchar* widget;
	GtkType type; /* type of widget */
	guint key;
	GdkModifierType mod;
	SpeechKey func;
	gpointer data;
};

struct _SpeechSet {
	gchar *name;
	GList *hooks;
};

struct _SpeechSound {
	gchar *name;
	gchar *file;
	gint   sample_id;
	gint   rate;
	gint   channels;
	gint   effect;
};

typedef enum {
	SPEECH_NULL,
	SPEECH_INTERNAL,
	SPEECH_TEXT,
	SPEECH_PARAM,
	SPEECH_ARG,
	SPEECH_SOUND,
	SPEECH_SCMD,
	SPEECH_TIP
} SpeechType;

typedef enum {
	STATE_BEGIN,
	STATE_HOOK,
	STATE_SERVER,
	STATE_SIGNAL,
	STATE_WIDGET,
	STATE_ACTION,
	STATE_INTERNAL,
	STATE_PARAM,
	STATE_ARG,
	STATE_TEXT,
	STATE_SOUND,
	STATE_SAMPLE,
	STATE_SET,
	STATE_INSTALL,
	STATE_REMOVE,
	STATE_LOAD,
	STATE_SETEL,
	STATE_SNOOP,
	STATE_CONFIG,
	STATE_SERVER_CMD,
	STATE_DESC,
	STATE_DACTION,
	STATE_ERROR
} SpeechState;

static GHashTable *speech_hooks = NULL;
static GHashTable *speech_config = NULL;
static GHashTable *speech_sets = NULL;
static GHashTable *speech_internals = NULL;
static GHashTable *speech_snoopers = NULL;
static GHashTable *descriptions = NULL;
static GHashTable *sound_samples = NULL;
static GHashTable *widget_queue = NULL;
static GList      *snooper_list = NULL;
static gint        need_snoop_try_again = 0;

static guint hook_delay_id = 0;
static esd_connection = -1;
SpeechServer * gspeech_server = NULL;


#ifdef USE_PERL
static PerlInterpreter *my_perl;
#endif

static void talk(gchar* str);

/* fix this mess:-) */
void gspeech_talk(char* str) {
	talk(str);
}

gchar*
gspeech_get_config(char* key) {
	return g_hash_table_lookup(speech_config, key);
}

int
gspeech_get_config_int(char* key, int def) {
	gchar * val = g_hash_table_lookup(speech_config, key);
	if (val)
		return atoi(val);
	return def;
}

int
gspeech_get_config_accel(char* key, guint *keyb, guint *keymod) {
	gchar *val = g_hash_table_lookup(speech_config, key);
	if (val) {
		gtk_accelerator_parse(val, keyb, keymod);
		return 1;
	} else {
		return 0;
	}
}

static void
speech_load_lib(gchar *fname) {
	GModule* module;
	gchar* path;
	gint i;
	SpeechIEntry* ientries = NULL;
	SpeechKEntry* kentries = NULL;
	SpeechServer** sentries = NULL;

	if (*fname == '/') {
		module = g_module_open(fname, G_MODULE_BIND_LAZY);
	} else {
		path = g_module_build_path(GSPEECHDIR "/gspeech", fname);
		module = g_module_open(path, G_MODULE_BIND_LAZY);
		g_free(path);
		if (!module) {
			path = g_module_build_path(NULL, fname);
			module = g_module_open(path, G_MODULE_BIND_LAZY);
			g_free(path);
		}
	}
	if (!module) {
		g_warning("Cannot load module %s", fname);
		return;
	}
	/* load internals, snooper and servers */
	if ( g_module_symbol(module, "speech_internals", (gpointer)&ientries) && ientries ) {
		for (i=0; ientries[i].name; ++i)
			g_hash_table_insert(speech_internals,
				ientries[i].name, ientries[i].func);
	}
	if ( g_module_symbol(module, "speech_snoopers", (gpointer)&kentries) && kentries ) {
		for (i=0; kentries[i].name; ++i)
			g_hash_table_insert(speech_snoopers,
				kentries[i].name, &kentries[i]);
	}
/*	if ( g_module_symbol(module, "speech_servers", (gpointer)&sentries) && sentries ) {
		for (i=0; sentries[i]->name; ++i)
			server_list = g_list_prepend(server_list, sentries[i]);
	}*/

}

/* FIXME: enable snoopers to work with try_again */
static gint
key_snooper(GtkWidget *w, GdkEventKey *event, gpointer func_data) {
	GtkType type;
	GtkType wfocus_child=0;
	GList *list;
	GtkWidget *orig=NULL;
	SpeechSnoop *snoop;
	int result = FALSE;

	if (!w)
		return FALSE;
	type = GTK_OBJECT(w)->klass->type;
	/*if ( GTK_IS_WINDOW(w) && (orig=GTK_WINDOW(w)->focus_widget) )
		wfocus_child = GTK_OBJECT(orig)->klass->type;*/
	if ( (orig=GTK_WINDOW(gtk_widget_get_toplevel(w))->focus_widget) )
		wfocus_child = GTK_OBJECT(orig)->klass->type;
	for (list = snooper_list; list; list = list->next) {
		snoop = list->data;
		g_assert(snoop != NULL);
		if (event->keyval != snoop->key || ((event->state & snoop->mod) != snoop->mod))
			continue;
		/*g_message("type (%s)",gtk_type_name(type));
		g_message("wfocus type (%s)",gtk_type_name(wfocus_child));*/
		/*g_message("snoop type (%s)", gtk_type_name(snoop->type));*/
		if (!orig)
			orig = w;
		if (!snoop->widget || (snoop->type 
		  && (gtk_type_is_a(type, snoop->type) || gtk_type_is_a(wfocus_child, snoop->type))) ) {
			snoop->func(orig, event, snoop->data);
			result = TRUE;
		}
		/*if (snoop->type && gtk_type_is_a(wfocus_child, snoop->type))
			snoop->func(orig, event, snoop->data);*/
	}

	return result;
}

static void
install_snoop(gchar* w, gchar* acc, gchar* action) {
	guint snoop_key, snoop_mod;
	static snooper_id = 0;
	SpeechSnoop *snoop;
	SpeechKey sfunc;
	SpeechKEntry *kentry;
	/* GtkKeySnoopFunc snooper */

	kentry = g_hash_table_lookup(speech_snoopers, action);
	if (!kentry) {
		g_warning("No snooper named %s", action);
		return;
	}
	gtk_accelerator_parse(acc, &snoop_key, &snoop_mod);
	if (!snooper_id)
		snooper_id = gtk_key_snooper_install(key_snooper, NULL);
	snoop = g_new0(SpeechSnoop, 1);

	snoop->func = kentry->func;
	snoop->data = kentry->data;
	snoop->key = snoop_key;
	snoop->mod = snoop_mod;
	if (!w || !*w) {
		snoop->widget = NULL;
		snoop->type = 0;
	} else {
		snoop->widget = g_strdup(w);
		snoop->type = gtk_type_from_name(snoop->widget);
		if (!snoop->type)
			need_snoop_try_again = 1;
	}

	snooper_list = g_list_prepend(snooper_list, snoop);
}

static gchar*
get_string_from_arg(GtkArg* arg) {
	/*g_message("Arg type: %s", gtk_type_name(arg->type));*/
	switch(arg->type) {
	case GTK_TYPE_STRING:
		return g_strdup(GTK_VALUE_STRING(*arg));
	case GTK_TYPE_CHAR:
	case GTK_TYPE_UCHAR:
		return g_strdup_printf("%c", (int)GTK_VALUE_UCHAR(*arg));
	case GTK_TYPE_BOOL:
		return g_strdup_printf("%s", GTK_VALUE_BOOL(*arg)?"true":"false");
	case GTK_TYPE_INT:
		return g_strdup_printf("%i", GTK_VALUE_INT(*arg));
	case GTK_TYPE_UINT:
		return g_strdup_printf("%u", GTK_VALUE_UINT(*arg));
	case GTK_TYPE_LONG:
		return g_strdup_printf("%li", GTK_VALUE_LONG(*arg));
	case GTK_TYPE_ULONG:
		return g_strdup_printf("%lu", GTK_VALUE_ULONG(*arg));
	case GTK_TYPE_FLOAT:
		return g_strdup_printf("%g", (double)GTK_VALUE_FLOAT(*arg));
	case GTK_TYPE_DOUBLE:
		return g_strdup_printf("%g", GTK_VALUE_DOUBLE(*arg));
	case GTK_TYPE_ENUM:
	case GTK_TYPE_FLAGS:
		/* should we handle these types also? */
	default:
	}
	return NULL;
}

static void
gspeech_play_sample (gchar *nick) {
	gchar *buf;
	SpeechSound *sound;

	if (esd_connection < 0)
		esd_connection = esd_open_sound(NULL);
	if (esd_connection < 0)
		return; /* fallback? */
	/*g_message("About to play %s", nick);*/
	if ( (sound=g_hash_table_lookup(sound_samples, nick)) ) {
		/* cache only one time for each gspeech process */
		if (sound->sample_id < 0) {
			sound->sample_id = esd_file_cache(esd_connection, "gspeech", sound->file);
			if (sound->sample_id < 0) {
				buf = g_strconcat(GSPEECHDIR, "/gspeech/", sound->file, NULL);
				sound->sample_id = esd_file_cache(esd_connection, "gspeech", buf);
				g_free(buf);
			}
		}
		if (sound->sample_id >= 0)
			esd_sample_play(esd_connection, sound->sample_id);
		else
			g_message("Cannot load sample %s", nick);
	} else {
		g_warning("No sample: %s", nick);
	}
	
}

void
server_cmd(char *text) {
	char *p, *val;
	int intval;

	p = strchr(text, ':');
	if (!p)
		return;

	*p = 0;
	val = p+1;
	if (!strcmp("speed", text)) {
		intval = atoi(val);
		if (*val == '+' || *val == '-')
			gspeech_server_change_speed(gspeech_server, intval);
		else
			gspeech_server_set_speed(gspeech_server, intval);
	} else if (!strcmp("voice", text)) {
		gspeech_server_set_voice(gspeech_server, val);
	} else if (!strcmp("mode", text)) {
		gspeech_server_set_mode(gspeech_server, val);
	} else if (!strcmp("lang", text)) {
		gspeech_server_set_language(gspeech_server, val);
	} else if (!strcmp("pitch", text)) {
		intval = atoi(val);
		if (*val == '+' || *val == '-')
			gspeech_server_change_pitch(gspeech_server, intval);
		else
			gspeech_server_set_pitch(gspeech_server, intval);
	}
	*p = ':';
}

void
describe_object (GtkObject* obj, int recurse) {
	GList* actions;
	SpeechData* sdata;
	GList *list;
	GtkArg arg;
	GtkType ptype;
	char *arg_string;
	char *tip = NULL;
	GtkTooltipsData * tdata;

	if (!obj)
		return;

	tdata = gtk_tooltips_data_get(GTK_WIDGET(obj));
	if (tdata)
		tip = tdata->tip_text;
	actions = (GList*)g_hash_table_lookup(descriptions, gtk_type_name(obj->klass->type));

	for (list = actions; list; list = list->next) {
		sdata = list->data;
		switch(sdata->type) {
		case SPEECH_TEXT:
			gspeech_talk(sdata->u.text);
			break;
		case SPEECH_ARG:
			arg.name = sdata->u.text;
			gtk_object_arg_get(obj, &arg, NULL);
			arg_string = get_string_from_arg(&arg);
			if ( arg_string ) {
				gspeech_talk(arg_string);
				g_free(arg_string);
			}
			break;
		case SPEECH_TIP:
			if (tip)
				gspeech_talk(tip);
			break;
		default:
			/* assert here */
			g_warning("Unknown description type %d", sdata->type);
		}
	}
	if (!actions) {
		/* use gtk_type_parent()? */
		arg_string = gtk_type_name(obj->klass->type);
		if (!strncmp("Gtk", arg_string, 3))
			arg_string += 3;
		arg_string = g_strdup_printf(" A %s  ", arg_string);
		gspeech_talk(arg_string);
		if (tip)
			gspeech_talk(tip);
		g_free(arg_string);
	}
	if (recurse-- > 0 && GTK_IS_CONTAINER(obj)) {
		gspeech_talk(" that contains: ");
		gtk_container_foreach(GTK_CONTAINER(obj), describe_object, GINT_TO_POINTER(recurse));
		gspeech_talk(" back one step ");
	}
}

static gboolean
perform_hook (gpointer data) {
	GList* actions;
	SpeechData* sdata;
	GList *list;
	
	actions = (GList*)data;

	for (list = actions; list; list = list->next) {
		sdata = list->data;
		switch(sdata->type) {
		case SPEECH_SOUND:
			gspeech_play_sample(sdata->u.text);
			break;
		case SPEECH_TEXT:
			talk(sdata->u.text);
			break;
		case SPEECH_SCMD:
			server_cmd(sdata->u.text);
			break;
		default:
			/* assert here */
			g_warning("Unknown speech type %d", sdata->type);
		}
		g_free(sdata->u.text);
		g_free(sdata);
	}
	if (actions) {
		g_list_free(actions);
	}
	hook_delay_id = 0;
	return 0;
}

/* FIXME: emission handler deve solo lanciare una funzione in timeout con
 * hook come argomento: e questa funzione diventa simile a emissione 
 * handler di adesso*/
static gboolean
emission_handler(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	SpeechHook* hook;
	SpeechData* sdata, *newsdata;
	GList *list;
	GList *actions = NULL;
	GList * sounds = NULL;
	GtkArg arg;
	gchar* arg_string;

	hook = (SpeechHook*)data;

	/* Work-around for emission brokeness */
	if (!gtk_type_is_a(obj->klass->type, hook->type))
		return TRUE;
	/*g_message("Handling %s", hook->name);*/

	/*if (GTK_IS_WIDGET(obj) && !GTK_WIDGET_MAPPED(obj)) {
		return TRUE;
	}*/


	for (list = hook->actions; list; list = list->next) {
		sdata = list->data;
		newsdata = g_new0(SpeechData, 1);
		newsdata->type = SPEECH_NULL;
		switch(sdata->type) {
		case SPEECH_SOUND:
			newsdata->type = sdata->type;
			newsdata->u.text = g_strdup(sdata->u.text);
			break;
		case SPEECH_TEXT:
			newsdata->type = sdata->type;
			newsdata->u.text = g_strdup(sdata->u.text);
			break;
		case SPEECH_PARAM:
			if (sdata->u.number >= n) /* emit a warning */
				break;
			arg_string = get_string_from_arg(&(args[sdata->u.number]));
			if ( arg_string ) {
				newsdata->type = SPEECH_TEXT;
				newsdata->u.text = arg_string;
			}
			break;
		case SPEECH_ARG:
			arg.name = sdata->u.text;
			gtk_object_arg_get(obj, &arg, NULL);
			arg_string = get_string_from_arg(&arg);
			if ( arg_string ) {
				newsdata->type = SPEECH_TEXT;
				newsdata->u.text = arg_string;
			}
			break;
		case SPEECH_INTERNAL:
			arg_string = sdata->u.func(obj, sig, n, args, data);
			if ( arg_string ) {
				newsdata->type = SPEECH_TEXT;
				newsdata->u.text = arg_string;
			}
			break;
		case SPEECH_TIP:
			{
			GtkTooltipsData * tdata;
			tdata = gtk_tooltips_get_data(obj);
			if (tdata && tdata->tip_text)
				arg_string = g_strdup(tdata->tip_text);
			if ( arg_string ) {
				newsdata->type = SPEECH_TEXT;
				newsdata->u.text = arg_string;
			}
			break;
			}
		case SPEECH_SCMD:
			break;
		default:
			g_warning("Unknown speech type %d for hook %s", sdata->type, hook->name);
		}
		if (newsdata->type == SPEECH_NULL)
			g_free(newsdata);
		else
			actions = g_list_append(actions, newsdata);
	}

	if (actions) {
		if (hook_delay_id)
			gtk_timeout_remove(hook_delay_id);
		hook_delay_id = gtk_timeout_add_full(gspeech_get_config_int("/prefs/delay", 100),
			(GtkFunction)perform_hook, NULL, actions, NULL);
	}
	return TRUE;
}

static int
install_set(gchar* set_name) {
	SpeechSet *set;
	SpeechHook *hook;
	GList * list;

	if (!(set = g_hash_table_lookup(speech_sets, set_name))) {
		g_warning("Set %s not defined", set_name);
		return 0;
	}
	list = set->hooks;

	for (;list; list= list->next) {
		hook = g_hash_table_lookup(speech_hooks, list->data);
		if (!hook) {
			g_warning("Hook %s not found", (gchar*)list->data);
			continue;
		}
		hook->active = 1;
		if ( !hook->type )
			hook->type = gtk_type_from_name(hook->widget);
		if ( !hook->type  && !g_hash_table_lookup(widget_queue, hook->widget) ) {
			g_hash_table_insert(widget_queue, hook->widget, NULL);
			continue;
		}
		if (hook->signal_id && hook->emission_id)
			continue; /* already installed */
		if (!hook->signal_id)
			hook->signal_id = gtk_signal_lookup(hook->signal, hook->type);
		if (!hook->signal_id) {
			g_warning("No signal %s for object %s", hook->signal, hook->widget);
			continue;
		}
		if (!hook->emission_id)
			hook->emission_id = gtk_signal_add_emission_hook(hook->signal_id, emission_handler, hook);
	}
	return 0;
}

static int
remove_set(gchar* set_name) {
	SpeechSet *set;
	SpeechHook *hook;
	GList * list;

	if (!(set = g_hash_table_lookup(speech_sets, set_name))) {
		g_warning("Set %s not defined", set_name);
		return 0;
	}
	list = set->hooks;

	while (list) {
		hook = g_hash_table_lookup(speech_hooks, list->data);
		if (!hook) {
			g_warning("Hook %s not found", (gchar*)list->data);
			list= list->next;
			continue;
		}
		hook->active = 0;
		if ( hook->emission_id ) {
			gtk_signal_remove_emission_hook(hook->signal_id, hook->emission_id);
			hook->emission_id = 0;
		}
		list= list->next;
	}
	return 0;
}

static int
configure_server (gchar *name, GScanner* scanner) {
	GList *list;
	SpeechServer *server=NULL;

	server = gspeech_server_new(name);
	if (!server) {
		g_warning("Unknown server %s", name);
		return STATE_ERROR;
	}
	gspeech_server_parse(server, NULL, scanner);
	gspeech_server_init(server);
	/* free previous... */
	if (gspeech_server) {
		gspeech_server_stop(gspeech_server);
		gspeech_server_destroy(gspeech_server);
	}
	gspeech_server = server;
	return STATE_BEGIN;

}

static void
do_config(gchar* filename) {
	FILE* f;
	GScanner* scanner;
	SpeechState state = STATE_BEGIN;
	GTokenValue val;
	SpeechData *sdata = NULL;
	SpeechHook *sinfo = NULL;
	SpeechSet *sset = NULL;
	SpeechFunc sfunc = NULL;
	SpeechSound *sound = NULL;
	int brace_count = 0;
	int tok;
	gchar *snoop_widget=NULL, *snoop_acc=NULL;
	gchar *desc_widget;
	GList * desc_list;

	if (!(f=fopen(filename, "r"))) {
		g_message("Cannot open %s", filename);
		return;
	}
	scanner = g_scanner_new(NULL);
	g_scanner_input_file(scanner, fileno(f));

	while ((tok=g_scanner_get_next_token(scanner)) != G_TOKEN_EOF ) {
		val = g_scanner_cur_value(scanner);
		switch(state) {
		case STATE_ERROR:
			if ( tok == G_TOKEN_LEFT_CURLY )
				brace_count++;
			else if ( tok == G_TOKEN_RIGHT_CURLY ) {
				brace_count--;
			}
			/* check ';' */
			if ( brace_count <= 0  || tok == ';' ) {
				brace_count = 0;
				state = STATE_BEGIN;
			}
			break;
		case STATE_BEGIN:
			if (tok == ';')
				break;
			if (tok != G_TOKEN_IDENTIFIER) {
				g_warning("Ident expected: %d", tok);
				state = STATE_ERROR;
				break;
			}
			if (!strcmp("hook", val.v_identifier)) {
				state = STATE_HOOK;
				break;
			} else if (!strcmp("set", val.v_identifier)) {
				state = STATE_SET;
				break;
			} else if (!strcmp("install", val.v_identifier)) {
				state = STATE_INSTALL;
				break;
			} else if (!strcmp("remove", val.v_identifier)) {
				state = STATE_REMOVE;
				break;
			} else if (!strcmp("snoop", val.v_identifier)) {
				state = STATE_SNOOP;
				break;
			} else if (!strcmp("config", val.v_identifier)) {
				state = STATE_CONFIG;
				break;
			} else if (!strcmp("sample", val.v_identifier)) {
				state = STATE_SAMPLE;
				break;
			} else if (!strcmp("load", val.v_identifier)) {
				state = STATE_LOAD;
				break;
			} else if (!strcmp("description", val.v_identifier)) {
				state = STATE_DESC;
				break;
			} else if (!strcmp("server", val.v_identifier)) {
				state = STATE_SERVER;
				break;
			}
			g_warning("Unknown command: %s", val.v_identifier);
			state = STATE_ERROR;
			break;
		case STATE_DESC:
			if (tok != G_TOKEN_STRING) {
				g_warning("description requires a widget name");
				state = STATE_ERROR;
				break;
			}
			desc_widget = g_strdup(val.v_string);
			desc_list = NULL;
			state = STATE_DACTION;
			if ((tok=g_scanner_get_next_token(scanner)) != G_TOKEN_LEFT_CURLY) {
				g_warning("Missing {");
				state = STATE_ERROR;
				break;
			}
			break;
		case STATE_DACTION:
			if (tok == ';')
				break;
			if (tok == G_TOKEN_RIGHT_CURLY) {
				if ( desc_list ) {
					/* check existing */
					g_hash_table_insert(descriptions, desc_widget, desc_list);
				} else {
					g_warning("description %s without actions", desc_widget);
				}
				desc_list = NULL;
				desc_widget = NULL;
				sdata = NULL;
				state = STATE_BEGIN;
				break;
			}
			if (tok == G_TOKEN_STRING) {
				sdata = g_new0(SpeechData, 1);
				sdata->type = SPEECH_TEXT;
				sdata->u.text = g_strdup(val.v_string);
				desc_list = g_list_append(desc_list, sdata);
				sdata = NULL;
			} else if (tok == G_TOKEN_IDENTIFIER) {
				/*if (!strcmp("server", val.v_identifier)) {
					state = STATE_SERVER_CMD;
					break;
				} else if (!strcmp("sound", val.v_identifier)) {
					state = STATE_SOUND;
					break;
				} else */
				if (!strcmp("arg", val.v_identifier)) {
					tok = g_scanner_get_next_token(scanner);
					val = g_scanner_cur_value(scanner);
					if (tok != G_TOKEN_STRING) {
						g_warning("snoop requires a key_sequnece");
						state = STATE_ERROR;
						break;
					}
					sdata = g_new0(SpeechData, 1);
					sdata->type = SPEECH_ARG;
					sdata->u.text = g_strdup(val.v_string);
					desc_list = g_list_append(desc_list, sdata);
					sdata = NULL;
					break;
				}/* else if (!strcmp("internal", val.v_identifier)) {
					state = STATE_INTERNAL;
					break;
				}*/ else {
					state = STATE_ERROR;
					break;
				}
			} else {
				state = STATE_ERROR;
				break;
			}
			break;
		case STATE_SNOOP:
			if (tok != G_TOKEN_STRING) {
				g_warning("snoop requires a widget name");
				state = STATE_ERROR;
				break;
			}
			if (snoop_widget)
				g_free(snoop_widget);
			snoop_widget = g_strdup(val.v_string);
			tok = g_scanner_get_next_token(scanner);
			val = g_scanner_cur_value(scanner);
			if (tok != G_TOKEN_STRING) {
				g_warning("snoop requires a key_sequnece");
				state = STATE_ERROR;
				break;
			}
			if (snoop_acc)
				g_free(snoop_acc);
			snoop_acc = g_strdup(val.v_string);
			tok = g_scanner_get_next_token(scanner);
			val = g_scanner_cur_value(scanner);
			if (tok != G_TOKEN_STRING) {
				g_warning("snoop requires a action name");
				state = STATE_ERROR;
				break;
			}
			install_snoop(snoop_widget, snoop_acc, val.v_string);
			state = STATE_BEGIN;
			break;
		case STATE_CONFIG:
			if (tok != G_TOKEN_STRING) {
				g_warning("config requires a string");
				state = STATE_ERROR;
				break;
			}
			{
				gchar *cval;
				gchar *key;
				key = g_strdup(val.v_string);
				cval = strchr(key, '=');
				if (cval)
					*cval++ = 0;
				/* FIXME: leak */
				g_free(g_hash_table_lookup(speech_config, key));
				g_hash_table_insert(speech_config, key, cval);
			}
			state = STATE_BEGIN;
			break;
		case STATE_SERVER:
			if (tok != G_TOKEN_STRING) {
				g_warning("server requires a string");
				state = STATE_ERROR;
				break;
			} else if (tok == G_TOKEN_RIGHT_CURLY) {
				state = STATE_BEGIN;
				break;
			}
			/* FIXME */
			state = configure_server(val.v_string, scanner);
			break;
		case STATE_LOAD:
			if (tok != G_TOKEN_STRING) {
				g_warning("load requires a library name to load");
				state = STATE_ERROR;
				break;
			}
			speech_load_lib(val.v_string);
			state = STATE_BEGIN;
			break;
		case STATE_REMOVE:
			if (tok != G_TOKEN_STRING) {
				g_warning("remove requires a set's name");
				state = STATE_ERROR;
				break;
			}
			remove_set(val.v_string);
			state = STATE_BEGIN;
			break;
		case STATE_INSTALL:
			if (tok != G_TOKEN_STRING) {
				g_warning("install requires a set's name");
				state = STATE_ERROR;
				break;
			}
			install_set(val.v_string);
			state = STATE_BEGIN;
			break;
		case STATE_SET:
			if (tok != G_TOKEN_STRING) {
				g_warning("set requires a set name");
				state = STATE_ERROR;
				break;
			}
			sset = g_new0(SpeechSet, 1);
			sset->name = g_strdup(val.v_string);
			tok = g_scanner_get_next_token(scanner);
			if ( tok != G_TOKEN_LEFT_CURLY ) {
				g_warning("Missing { for set %s", val.v_string);
				g_free(sset->name);
				g_free(sset);
				sset = NULL;
				state = STATE_ERROR;
				break;
			}
			state = STATE_SETEL;
			break;
		case STATE_SETEL:
			if ( tok == G_TOKEN_RIGHT_CURLY ) {
				if ( sset->hooks ) {
					/* check existing */
					g_hash_table_insert(speech_sets, sset->name, sset);
				} else {
					g_warning("No hooks for set %s", sset->name);
					/* free sset */
				}
				sset = NULL;
				state = STATE_BEGIN;
				break;
			}
			if ( tok != G_TOKEN_STRING ) {
				g_warning("set needs a hook name");
				break;
			}
			/* order is not important in sets */
			/* we should check duplicates, though */
			sset->hooks = g_list_prepend(sset->hooks, g_strdup(val.v_string));
			break;
		case STATE_SAMPLE:
			if (tok != G_TOKEN_STRING) {
				g_warning("Needs a sample name");
				state = STATE_ERROR;
				break;
			}
			sound = g_new0(SpeechSound, 1);
			sound->name = g_strdup(val.v_string);
			tok = g_scanner_get_next_token(scanner);
			val = g_scanner_cur_value(scanner);
			if (tok != G_TOKEN_STRING) {
				g_warning("Needs a sample filename");
				g_free(sound->name);
				g_free(sound);
				state = STATE_ERROR;
				break;
			}
			sound->file = g_strdup(val.v_string);
			sound->sample_id = -1;
			/* FIXME: leak: delete old entry */
			g_hash_table_insert(sound_samples, sound->name, sound);
			/*g_warning("Added sample: %s -> %s", sound->name, sound->file);*/
			state = STATE_BEGIN;
			break;
		case STATE_HOOK:
			if (tok != G_TOKEN_STRING) {
				g_warning("Needs a hook name");
				state = STATE_ERROR;
				break;
			}
			sinfo = g_new0(SpeechHook, 1);
			sinfo->name = g_strdup(val.v_string);
			state = STATE_WIDGET;
			break;
		case STATE_WIDGET:
			if (tok != G_TOKEN_STRING) {
				g_warning("Needs a widget name");
				state = STATE_ERROR;
				break;
			}
			sinfo->widget = g_strdup(val.v_string);
			state = STATE_SIGNAL;
			break;
		case STATE_SIGNAL:
			if (tok != G_TOKEN_STRING) {
				g_warning("Needs a signal name");
				state = STATE_ERROR;
				break;
			}
			sinfo->signal = g_strdup(val.v_string);
			state = STATE_ACTION;
			if ((tok=g_scanner_get_next_token(scanner)) != G_TOKEN_LEFT_CURLY) {
				g_warning("Missing {");
				state = STATE_ERROR;
				break;
			}
			break;
		case STATE_ACTION:
			if (tok == ';')
				break;
			if (tok == G_TOKEN_RIGHT_CURLY) {
				if ( sinfo->actions ) {
					/* check existing */
					g_hash_table_insert(speech_hooks, sinfo->name, sinfo);
				} else {
					g_warning("hook %s without actions", sinfo->name);
				}
				sinfo = NULL;
				state = STATE_BEGIN;
				break;
			}
			if (tok == G_TOKEN_STRING) {
				sdata = g_new0(SpeechData, 1);
				sdata->type = SPEECH_TEXT;
				sdata->u.text = g_strdup(val.v_string);
				sinfo->actions = g_list_append(sinfo->actions, sdata);
				sdata = NULL;
				state = STATE_ACTION;
			} else if (tok == G_TOKEN_IDENTIFIER) {
				if (!strcmp("param", val.v_identifier)) {
					state = STATE_PARAM;
					break;
				} else if (!strcmp("server", val.v_identifier)) {
					state = STATE_SERVER_CMD;
					break;
				} else if (!strcmp("sound", val.v_identifier)) {
					state = STATE_SOUND;
					break;
				} else if (!strcmp("arg", val.v_identifier)) {
					state = STATE_ARG;
					break;
				} else if (!strcmp("tip", val.v_identifier)) {
					sdata = g_new0(SpeechData, 1);
					sdata->type = SPEECH_TIP;
					sinfo->actions = g_list_append(sinfo->actions, sdata);
					sdata = NULL;
					break;
				} else if (!strcmp("internal", val.v_identifier)) {
					state = STATE_INTERNAL;
					break;
				} else {
					state = STATE_ERROR;
					break;
				}
			} else {
				state = STATE_ERROR;
				break;
			}
			break;
		case STATE_SERVER_CMD:
			if (tok != G_TOKEN_STRING) {
				g_warning("server requires a string");
				state = STATE_ERROR;
				break;
			}
			sdata = g_new0(SpeechData, 1);
			sdata->type = SPEECH_SCMD;
			sdata->u.text = g_strdup(val.v_string);
			sinfo->actions = g_list_append(sinfo->actions, sdata);
			sdata = NULL;
			state = STATE_ACTION;
			break;
		case STATE_SOUND:
			if (tok != G_TOKEN_STRING) {
				g_warning("sound requires a string");
				state = STATE_ERROR;
				break;
			}
			sdata = g_new0(SpeechData, 1);
			sdata->type = SPEECH_SOUND;
			sdata->u.text = g_strdup(val.v_string);
			sinfo->actions = g_list_append(sinfo->actions, sdata);
			sdata = NULL;
			state = STATE_ACTION;
			break;
		case STATE_INTERNAL:
			if (tok != G_TOKEN_STRING) {
				g_warning("internal requires a string");
				state = STATE_ERROR;
				break;
			}
			sfunc = g_hash_table_lookup(speech_internals, val.v_string);
			if ( sfunc ) {
				sdata = g_new0(SpeechData, 1);
				sdata->type = SPEECH_INTERNAL;
				sdata->u.func = sfunc;
				sinfo->actions = g_list_append(sinfo->actions, sdata);
				sdata = NULL;
			} else {
				g_warning("Unknows internal %s", val.v_string);
			}
			
			state = STATE_ACTION;
			break;
		case STATE_PARAM:
			if ( tok != G_TOKEN_INT ) {
				g_warning("param requires a int");
				state = STATE_ERROR;
				break;
			}
			sdata = g_new0(SpeechData, 1);
			sdata->type = SPEECH_PARAM;
			sdata->u.number = val.v_int;
			sinfo->actions = g_list_append(sinfo->actions, sdata);
			sdata = NULL;
			state = STATE_ACTION;
			break;
		case STATE_ARG:
			if (tok != G_TOKEN_STRING) {
				g_warning("arg requires a string");
				state = STATE_ERROR;
				break;
			}
			sdata = g_new0(SpeechData, 1);
			sdata->type = SPEECH_ARG;
			sdata->u.text = g_strdup(val.v_string);
			sinfo->actions = g_list_append(sinfo->actions, sdata);
			sdata = NULL;
			state = STATE_ACTION;
			break;
		default:
			g_warning("unknow state %d", state);
			state = STATE_ERROR;
		}
	}

	/* free sset, sdata, sinfo */
	if ( state != STATE_BEGIN )
		g_warning("Statetment not complete %d", state);
	g_scanner_destroy(scanner);
	fclose(f);
}

typedef void (*scale_draw)(GtkScale*);

static scale_draw vscale_backup=NULL;
static scale_draw hscale_backup=NULL;

static void
vscale_hack(GtkScale * scale) {
	double val = GTK_RANGE(scale)->adjustment->value;
	gtk_signal_emit_by_name(GTK_OBJECT(scale), "my_draw_value", val);
	vscale_backup(scale);
}

static void
hscale_hack(GtkScale * scale) {
	double val = GTK_RANGE(scale)->adjustment->value;
	gtk_signal_emit_by_name(GTK_OBJECT(scale), "my_draw_value", val);
	hscale_backup(scale);
}

typedef (*castf) (GtkObject*, double, gpointer);

void
my_gtk_marshal_NONE_DOUBLE (GtkObject* object, GtkSignalFunc func, gpointer funcd, GtkArg* args) {
	castf cf;

	cf = func;
	(*cf)(object, GTK_VALUE_DOUBLE(args[0]), funcd);
}

static void 
gspeech_hacks () {
	/* create a signal for scale widgets: this works for scale widgets
	 * that draw the value somewhere in the widget. The hack is to create
	 * a user signal that can then be used as usual in the config file
	 * and patching at runtime the Gtk[HV]Scale classes */
	GtkScaleClass * klass;

	klass = gtk_type_class(gtk_scale_get_type());

	gtk_object_class_user_signal_new(klass, "my_draw_value",
			GTK_RUN_LAST,
			my_gtk_marshal_NONE_DOUBLE,
			GTK_TYPE_NONE, 1, GTK_TYPE_DOUBLE);
	klass = gtk_type_class(gtk_vscale_get_type());
	vscale_backup = klass->draw_value;
	klass->draw_value = vscale_hack;
	klass = gtk_type_class(gtk_hscale_get_type());
	hscale_backup = klass->draw_value;
	klass->draw_value = hscale_hack;
}

static GList * spoken_queue = NULL;

static void
talk(char* text) {
	gchar * last = NULL;
	static time_t last_time;
	GList* tmp;

	time_t now = time(NULL);

	if (spoken_queue)
		last = spoken_queue->data;
	/* skip if we said the same thing less then 2 seconds ago and
	   the text is not a single char */
	if (last && ( strlen(text) > 1 && !strcmp(text, last) && now-last_time < 2 ))
		return;
	last = g_strdup(text);
	spoken_queue = g_list_prepend(spoken_queue, last);
	last_time = now;
	/* g_message("Talking: %s", text); */
	/* Save in a fifo some sentences */
	if ( gspeech_server )
		gspeech_server_say(gspeech_server, text);
	if (g_list_length(spoken_queue) > gspeech_get_config_int("/prefs/queue-length", 8)) {
		tmp = g_list_last(spoken_queue);
		g_free(tmp->data);
		tmp->prev->next=NULL;
		tmp->prev=NULL;
		g_list_free(tmp);
	}
}

void
gspeech_repeat_last () {
	GList *tmp;

	for (tmp = g_list_last(spoken_queue); tmp; tmp=tmp->prev) {
		gspeech_server_say(gspeech_server, tmp->data);
	}
}

/* this stuff is what enables gspeech to handle unknown objects */
static void
try_again(gpointer key, gpointer data, gpointer udata) {
	SpeechHook * hook;

	hook = (SpeechHook*)data;
	if (hook->type)
		return;
	if (udata && strcmp(udata, hook->widget))
		return;
	hook->type = gtk_type_from_name(hook->widget);
	if ( !hook->type )
		return;

	if ( g_hash_table_lookup(widget_queue, hook->widget)) {
		g_hash_table_remove(widget_queue, hook->widget);
		g_message("%s has been created finally!", hook->widget);
	}
	
	hook->signal_id = gtk_signal_lookup(hook->signal, hook->type);
	if (!hook->signal_id) {
		g_warning("No signal %s for object %s", hook->signal, hook->widget);
		return;
	}

	if (hook->active)
		hook->emission_id = gtk_signal_add_emission_hook(hook->signal_id, emission_handler, hook);
	return;

}

static void
snooper_try_again(gpointer data, gpointer user_data) {
	SpeechSnoop *snoop = (SpeechSnoop*)data;
	if (!snoop->widget || snoop->type)
		return;
	snoop->type = gtk_type_from_name(snoop->widget);
	if (!snoop->type)
		need_snoop_try_again = 1;
}

#if 0
static gboolean
creation_hook(GtkTypeQuery* query, gpointer data) {

	if (!g_hash_table_size(widget_queue) && !need_snoop_try_again)
		return FALSE;
	if (!g_hash_table_lookup(widget_queue, query->type_name))
		return TRUE;

	g_hash_table_foreach(speech_hooks, try_again, query->type_name);
	if ( need_snoop_try_again ) {
		need_snoop_try_again=0;
		g_list_foreach(snooper_list, snooper_try_again, NULL);
	}
	return TRUE;
}
#else
static gboolean
idle_jerk(gpointer data) {
	if (!g_hash_table_size(widget_queue) && !need_snoop_try_again)
		return FALSE;
	g_hash_table_foreach(speech_hooks, try_again, NULL);
	if ( need_snoop_try_again ) {
		need_snoop_try_again=0;
		g_list_foreach(snooper_list, snooper_try_again, NULL);
	}
	return GPOINTER_TO_INT(data);

}
#endif

int 
gtk_module_init(gint argc, char* argv[]) {
	int i;
	gchar *user_config, *home;

	speech_config = g_hash_table_new(g_str_hash, g_str_equal);
	speech_sets = g_hash_table_new(g_str_hash, g_str_equal);
	speech_hooks = g_hash_table_new(g_str_hash, g_str_equal);
	speech_internals = g_hash_table_new(g_str_hash, g_str_equal);
	speech_snoopers = g_hash_table_new(g_str_hash, g_str_equal);
	widget_queue = g_hash_table_new(g_str_hash, g_str_equal);
	sound_samples = g_hash_table_new(g_str_hash, g_str_equal);
	descriptions = g_hash_table_new(g_str_hash, g_str_equal);

	/* Change hash tables to contain all of the structure, not only the func */
	for (i=0; gspeech_default_internals[i].name; ++i)
		g_hash_table_insert(speech_internals,
			gspeech_default_internals[i].name, gspeech_default_internals[i].func);
	for (i=0; gspeech_default_snoopers[i].name; ++i)
		g_hash_table_insert(speech_snoopers,
			gspeech_default_snoopers[i].name, &gspeech_default_snoopers[i]);

	gspeech_hacks();

	do_config(GSPEECHCONFIG "/gspeech.rc");
	home = g_get_home_dir();
	user_config = g_strconcat(home, "/.gspeechrc", NULL);
	do_config(user_config);
	g_free(home);
	g_free(user_config);
	home = g_get_prgname();
	user_config =  g_strconcat(GSPEECHDIR "/gspeech/", home, ".rc", NULL);
	do_config(user_config);
	g_free(home);
	g_free(user_config);
	/* automatic loading of app-specific config */
	/* only when debugging */
	do_config("gspeech.rc");

	if (!gspeech_server)
		g_warning("No speech server configured");
	talk("Welcome to G-Speech\n");	

#ifdef USE_PERL
	my_perl = perl_alloc();
	perl_construct( my_perl );
	perl_parse(my_perl, NULL, 3, embedding, NULL);
	perl_run(my_perl);

#endif

#if 0
	gtk_type_add_creation_hook(creation_hook, NULL, NULL);
#else
	/* one-shot idle */
	gtk_idle_add(idle_jerk, GINT_TO_POINTER(0));
	gtk_timeout_add(2000, idle_jerk, GINT_TO_POINTER(1));
#endif

	return 0;
}

