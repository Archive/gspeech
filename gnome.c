#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <zvt/zvtterm.h>
#include "gspeech.h"

enum {
	GS_CHAR,
	GS_WORD,
	GS_LINE,
	GS_NLINES,
	GS_POSLINE
};

static gchar*
zvt_get_text(ZvtTerm*term, int mode) {
	struct vt_em *vem;
	gchar *res;
	guint sxpos, expos, sypos, eypos;
	gint len;

	vem = &term->vx->vt;
	switch(mode) {
	case GS_CHAR:
		sxpos=vem->cursorx;
		sypos=vem->cursory;
		expos=vem->cursorx+1;
		eypos=vem->cursory;
		break;
	case GS_WORD:
	case GS_LINE:
	default:
		sxpos=0;
		sypos=vem->cursory;
		expos=128;
		eypos=vem->cursory;
		break;
	}
	res = zvt_term_get_buffer(term, NULL, 
		VT_SELTYPE_CHAR, sxpos, sypos, expos, eypos);
	return res;
}

static gchar*
zvt_read(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	ZvtTerm* term;
	GdkEventKey* kevent;
	GdkEvent* event;
	gchar* val;
	static guint rw_key=0;
	static guint rw_mod=0;
	static guint rl_key=0;
	static guint rl_mod=0;
	static guint rc_key=0;
	static guint rc_mod=0;

	if (!ZVT_IS_TERM(obj))
		return NULL;
	term = ZVT_TERM(obj);

	if (n < 1 || args[0].type != GTK_TYPE_GDK_EVENT)
		return NULL;
	event = GTK_VALUE_POINTER(args[0]);
	if ( event->type != GDK_KEY_PRESS )
		return NULL;
	kevent = (GdkEventKey*)event;
	if (!rw_key) {
		if ( (val = gspeech_get_config("/bind/read_word")) )
			gtk_accelerator_parse(val, &rw_key, &rw_mod);
	}
	if (!rl_key) {
		if ( (val = gspeech_get_config("/bind/read_line")) )
			gtk_accelerator_parse(val, &rl_key, &rl_mod);
	}
	if (!rc_key) {
		if ( (val = gspeech_get_config("/bind/read_char")) )
			gtk_accelerator_parse(val, &rc_key, &rc_mod);
	}
	/*g_message("Got %c %d anded %d \nConfig %c %d", kevent->keyval, kevent->state);*/
	if ( kevent->keyval == rw_key && (kevent->state & rw_mod) == rw_mod )
		return zvt_get_text(term, GS_WORD);
	if ( kevent->keyval == rl_key && (kevent->state & rl_mod) == rl_mod )
		return zvt_get_text(term, GS_LINE);
	if ( kevent->keyval == rc_key && (kevent->state & rc_mod) == rc_mod )
		return zvt_get_text(term, GS_CHAR);
	/* return key_presses */
	return NULL;
}

SpeechIEntry 
speech_internals[] = {
	{"zvt_read", zvt_read, "Enables shortcuts to read the contents of a terminal emulator."},
	{NULL}
};
