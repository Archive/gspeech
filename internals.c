/* Internal functions of the GSpeech module */
#include <gtk/gtk.h>
#include "gspeech.h"
#include "gspeech-servers.h"

extern SpeechServer * gspeech_server;

enum {
	GS_CHAR,
	GS_WORD,
	GS_LINE,
	GS_NLINES,
	GS_POSLINE
};

static void 
read_child_label(GtkWidget* w, gpointer data) {
	gchar* text=NULL;
	GString *str = (GString*)data;
	
	if ( GTK_IS_LABEL(w)  && GTK_WIDGET_VISIBLE(w) ) {
		gtk_label_get(GTK_LABEL(w), &text);
		g_string_append_c(str, ' ');
		g_string_append(str, text);
	} else if ( GTK_IS_CONTAINER(w) ) {
		gtk_container_foreach(GTK_CONTAINER(w), read_child_label, data);
	}
}

static gchar*
read_label(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	GString *str;
	gchar *result = NULL;
	str = g_string_new("");
	
	read_child_label(GTK_WIDGET(obj), str);
	if ( *(str->str) )
		result = g_strdup(str->str);
	g_string_free(str, 1);
	return result;
}

static gchar*
read_clist_selection(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	gchar *result = NULL;
	
	if ( GTK_IS_CLIST(obj) && GTK_WIDGET_VISIBLE(obj) ) {
		gtk_clist_get_text(GTK_CLIST(obj), GTK_VALUE_INT(args[0]), GTK_VALUE_INT(args[1]), &result);
	}
	if ( result && *result ) {
		return g_strdup(result);
	} else
		return NULL;
}

static gchar*
read_ctree_selection(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	gchar *result = NULL;
	gint column;
	
	column = GTK_VALUE_INT(args[1]);
	if (column < 0)
		column = 0;
	if ( !GTK_IS_CTREE(obj) || !GTK_WIDGET_VISIBLE(obj) )
		return NULL;
	do {
		/* do the same for clist */
		gtk_ctree_node_get_text(GTK_CTREE(obj), GTK_VALUE_POINTER(args[0]), column, &result);
		if (!result)
			gtk_ctree_node_get_pixtext(GTK_CTREE(obj), GTK_VALUE_POINTER(args[0]), column, &result, NULL, NULL, NULL);
	} while (!result && ++column < GTK_CLIST(obj)->columns);

	/* return expanded status */
	if ( result && *result ) {
		return g_strdup(result);
	} else
		return NULL;
}

static gchar*
read_entry(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	gchar *result = NULL;
	
	if ( GTK_IS_ENTRY(obj) && GTK_WIDGET_VISIBLE(obj) ) {
		result = gtk_entry_get_text(GTK_ENTRY(obj));
	}
	if ( result && *result ) {
		return g_strdup(result);
	} else
		return NULL;
}

static gchar*
editable_insert(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	gchar *result = NULL;
	gint size;

	if ( !GTK_IS_EDITABLE(obj) || !GTK_WIDGET_VISIBLE(obj))
		return NULL;
	/* check args */
	size = GTK_VALUE_INT(args[1]);
	/* read only short strings */
	if (size <= 0)
		return NULL;
	if (size > 16 )
		size = 16;
	result = g_malloc0(size+1);
	memcpy(result, GTK_VALUE_STRING(args[0]), size);
	result[size] = 0;
	return result;
}

static gchar*
editable_get_text(GtkEditable* editable, int mode) {
	guint curpos;
	gchar* res;
	gint spos;
	gint epos;
	gchar* action;

	g_message("mode: %d", mode);
	curpos = gtk_editable_get_position(editable);

	if (mode == GS_LINE && GTK_IS_ENTRY(editable)) {
		res = g_strdup(gtk_entry_get_text(GTK_ENTRY(editable)));
		return res;
	}

	if ( mode == GS_CHAR ) {
		res = gtk_editable_get_chars(editable, curpos, curpos+1);
		g_message("char mode: %d", mode);
		return res;
	}

	action = "move_word"; /* FIXME: line handling */
	gtk_signal_emit_by_name(GTK_OBJECT(editable), action, -1);
	spos = gtk_editable_get_position(editable);
	gtk_editable_set_position(editable, curpos);
	gtk_signal_emit_by_name(GTK_OBJECT(editable), action, 1);
	epos = gtk_editable_get_position(editable);
	res = gtk_editable_get_chars(editable, spos, epos);

	gtk_editable_set_position(editable, curpos);
	g_message("GET_TEXT: '%s' %d (%d-%d)", res, curpos, spos, epos);
	return res;
}

static gchar*
editable_read(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	GtkEditable* editable;
	GdkEventKey* kevent;
	GdkEvent* event;
	gchar* val;
	static guint rw_key=0;
	static guint rw_mod=0;
	static guint rl_key=0;
	static guint rl_mod=0;
	static guint rc_key=0;
	static guint rc_mod=0;

	g_message("editable_read");
	if (!GTK_IS_EDITABLE(obj))
		return NULL;
	editable = GTK_EDITABLE(obj);

	if (n < 1 || args[0].type != GTK_TYPE_GDK_EVENT)
		return NULL;
	event = GTK_VALUE_POINTER(args[0]);
	if ( event->type != GDK_KEY_PRESS )
		return NULL;
	kevent = (GdkEventKey*)event;
	if (!rw_key)
		gspeech_get_config_accel("/bind/read_word", &rw_key, &rw_mod);
	if (!rl_key) 
		gspeech_get_config_accel("/bind/read_line", &rl_key, &rl_mod);
	if (!rc_key) 
		gspeech_get_config_accel("/bind/read_char", &rc_key, &rc_mod);
	/*g_message("keyname (%s)", gtk_accelerator_name(kevent->keyval, kevent->state));*/
	/*g_message("Got %c %d anded %d \nConfig %c %d", kevent->keyval, kevent->state, kevent->state & rw_mod, rw_key, rw_mod);*/
	if ( kevent->keyval == rw_key && (kevent->state & rw_mod) == rw_mod )
		return editable_get_text(editable, GS_WORD);
	if ( kevent->keyval == rl_key && (kevent->state & rl_mod) == rl_mod )
		return editable_get_text(editable, GS_LINE);
	if ( kevent->keyval == rc_key && (kevent->state & rc_mod) == rc_mod )
		return editable_get_text(editable, GS_CHAR);
	return NULL;
}

static gchar*
find_desc(GtkObject* obj, guint sig, guint n, GtkArg *args, gpointer data) {
	GtkWidget *w, *parent, *current;
	gchar *text;
	int steps = 0;

	/* g_message("find_desc"); */
	
	if (!GTK_IS_WIDGET(obj))
		return NULL;
	w = GTK_WIDGET(obj);
	parent = w->parent;
	while (parent && steps < 3) {
		if (GTK_IS_FRAME(parent) && GTK_FRAME(parent)->label) {
			/*g_message("found frame");*/
			return g_strdup(GTK_FRAME(parent)->label);
		} else if (GTK_IS_BOX(parent)) {
			GList *children;
			GtkBoxChild *bc;

			/*g_message("found box");*/
			children = GTK_BOX(parent)->children;
			text = NULL;
			while ( children  ) {
				bc = (GtkBoxChild*)children->data;
				if (bc->widget == w)
					break;
				if (GTK_IS_LABEL(bc->widget))
					gtk_label_get(GTK_LABEL(bc->widget), &text);
				children = children->next;
			}
			if (text) {
				/*g_message("found label in box");*/
				return g_strdup(text);
			 } else {
			 	/*g_message("Stepping up");*/
				w = parent;
				parent = w->parent;
				steps += 2;
				continue;
			}
		} else if (GTK_IS_TABLE(parent)) {
			GList *children;
			GtkTableChild *tc;
			gint left_attach, top_attach;

			/*g_message("found table");*/
			children = GTK_TABLE(parent)->children;
			while ( children  ) {
				tc = (GtkTableChild*)children->data;
				if (tc->widget == w) {
					left_attach = tc->left_attach;
					top_attach = tc->top_attach;
					break;
				}
				children = children->next;
			}
			children = GTK_TABLE(parent)->children;
			while ( children  ) {
				tc = (GtkTableChild*)children->data;
				if ( tc->top_attach == top_attach && tc->right_attach == left_attach && GTK_IS_LABEL(tc->widget)) {
					gtk_label_get(GTK_LABEL(tc->widget), &text);
					return g_strdup(text);
				}
				children = children->next;
			}
			/*g_message("Stepping up");*/
			w = parent;
			parent = w->parent;
			steps += 2;
			continue;
		}
		g_message("find_desc cannot handle %s", gtk_type_name(GTK_OBJECT(parent)->klass->type));
		w = parent;
		parent = w->parent;
		++steps;
	}
	/*g_message("find_desc failed");*/
	return NULL;
}

static gchar*
editable_get_chunk(GtkEditable *ed, guint curpos, char type) {
	gchar * res, *sp, *se, *text;
	gint pstart;

	/* make ints configurable */
	if (type == 'w') {
		if (curpos > 20)
			pstart = curpos - 20;
		else pstart = 0;
		res = gtk_editable_get_chars(ed, pstart, curpos + 20);
		g_message("request chars %d - %d: %s", pstart, curpos, res);
		if (!res)
			return NULL;
		sp = res + curpos;
		while ( sp >= res ) {
			if (!isalnum(*sp))
				break;
			--sp;
		}
		if (sp < res) sp = res;
		while (*sp && !isalnum(*sp))
			++sp;
		se = sp;
		while (*se && isalnum(*se))
			++se;
		*se = 0;
	} else {
		if (curpos > 80)
			pstart = curpos - 80;
		else pstart = 0;
		res = gtk_editable_get_chars(ed, pstart, curpos + 80);
		g_message("request chars %d - %d: %s", pstart, curpos, res);
		if (!res)
			return NULL;
		sp = res + curpos;
		while ( sp >= res ) {
			if ( *sp == '\n')
				break;
			--sp;
		}
		if (sp < res) sp = res;
		se = res + curpos;
		while (*se) {
			if ( *se == '\n') {
				*se = 0;
				break;
			}
			++se;
		}
	}
	text = g_strdup(sp);
	g_free(res);
	g_message("got: '%s'\n", text);
	return text;
}

static gint
read_editable(GtkWidget* w, GdkEventKey* e, gpointer data) {
	gchar * type = (gchar*)data;
	gchar *text;
	guint curpos;

	if (!GTK_IS_EDITABLE(w))
		return FALSE;
		
	/*g_message("read_editable: %s", type);*/
	if (!type)
		type = "line";

	curpos = gtk_editable_get_position(GTK_EDITABLE(w));

	switch(*type) {
		case 'c':
			text = gtk_editable_get_chars(GTK_EDITABLE(w), curpos, curpos+1);
			break;
		case 'w':
		case 'l':
		default:
			text = editable_get_chunk(GTK_EDITABLE(w), curpos, *type);
	}
	if (text) {
		gspeech_talk(text);
		g_free(text);
	}
	return TRUE;
}

static gint
server_control (GtkWidget* w, GdkEventKey* e, gpointer data) {
	gchar * type = (gchar*)data;
	int speed_change = gspeech_get_config_int("/prefs/speed-rate-change", 5);
	
	switch(*type) {
	case 'i':
		gspeech_server_change_speed(gspeech_server, speed_change);
		break;
	case 'd':
		gspeech_server_change_speed(gspeech_server, -speed_change);
		break;
	case 's':
		gspeech_server_stop(gspeech_server);
		break;
	case 'r':
		gspeech_repeat_last();
		break;
	}
	return TRUE;
}

static gint
describe_interface (GtkWidget* w, GdkEventKey* e, gpointer data) {
	char *type = (char*)data;
	GtkWidget* toplevel;
	if (*type == 'c') {
		describe_object(GTK_OBJECT(w), 0);
	} else if (*type == 'p') {
		toplevel = w->parent;
		while (toplevel) {
			describe_object(GTK_OBJECT(toplevel), 0);
			toplevel = toplevel->parent;
		}
	} else if (*type == 'w') {
		toplevel = gtk_widget_get_toplevel(w);
		describe_object(GTK_OBJECT(toplevel), gspeech_get_config_int("/prefs/description-depth", 2));
	}
	return TRUE;
}

static gint
guess_desc (GtkWidget* w, GdkEventKey* e, gpointer data) {
	char *desc;
	
	desc = find_desc(GTK_OBJECT(w), 0, 0, NULL, NULL);
	if (desc) {
		gspeech_talk(desc);
		g_free(desc);
	}
	return TRUE;
}

SpeechKEntry 
gspeech_default_snoopers[] = {
	{"read_line", read_editable, "Read the current line in an editable widget", "line"},
	{"read_word", read_editable, "Read the current word in an editable widget", "word"},
	{"read_char", read_editable, "Read the current char in an editable widget", "char"},
	{"server_faster", server_control, "Make the speech server talk faster", "i"},
	{"server_slower", server_control, "Make the speech server talk slower", "d"},
	{"server_stop", server_control, "Make the speech server stop talking", "s"},
	{"server_repeat", server_control, "Make the speech server repeat the last words", "r"},
	{"describe", describe_interface, "Describe the current widget", "c"},
	{"describe_window", describe_interface, "Describe the current window", "w"},
	{"describe_parent", describe_interface, "Describe the parent hierarchy of the current widget", "p"},
	{"guess_desc", guess_desc, "Try to find a description of the current widget", ""},
	{NULL}
};


SpeechIEntry 
gspeech_default_internals[] = {
	{"read_label", read_label, "This reads le labels visible in the widget"},
	{"read_entry", read_entry, "This reads the content of an entry widget"},
	{"clist_select", read_clist_selection, "This reads the content of an item in a columned list"},
	{"ctree_select", read_ctree_selection, "This reads the content of an item in a columned tree"},
	{"editable_insert", editable_insert, "This gives reads the letter you type in an editable widget"},
	{"editable_read", editable_read, "Enables shortcuts to read chars, words, lines..."},
	{"find_desc", find_desc, "Tries to find a description of the current widget"},
	{NULL}
};

