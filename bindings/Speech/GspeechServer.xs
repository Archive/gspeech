#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <gspeech-servers.h>

typedef SpeechServer* Text__GspeechServer;

static int
not_here(char *s)
{
    croak("%s not implemented on this architecture", s);
    return -1;
}

static double
constant(char *name, int arg)
{
    errno = 0;
    switch (*name) {
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}


MODULE = Text::GspeechServer	PACKAGE = Text::GspeechServer		PREFIX = gspeech_server_


double
constant(name,arg)
	char *		name
	int		arg

Text::GspeechServer
new (Class, name)
	SV *Class
	char*	name
	CODE:
	RETVAL = gspeech_server_new(name);
	OUTPUT:
	RETVAL

void
gspeech_server_say (server, text)
	Text::GspeechServer	server
	char*	text

void
gspeech_server_stop (server)
	Text::GspeechServer	server

void
gspeech_server_init (server)
	Text::GspeechServer	server

void
gspeech_server_parse (server, text)
	Text::GspeechServer	server
	char*	text
	CODE:
	gspeech_server_parse(server, text, NULL);

void
gspeech_server_destroy (server)
	Text::GspeechServer	server

void
gspeech_server_set_language (server, lang)
	Text::GspeechServer	server
	char*	lang

void
gspeech_server_set_voice (server, voice)
	Text::GspeechServer	server
	char*	voice

void
gspeech_server_set_mode (server, mode)
	Text::GspeechServer	server
	char*	mode

void
gspeech_server_set_speed (server, speed)
	Text::GspeechServer	server
	int	speed

void
gspeech_server_set_pitch (server, pitch)
	Text::GspeechServer	server
	int	pitch

void
gspeech_server_change_speed (server, diff)
	Text::GspeechServer	server
	int	diff

void
gspeech_server_change_pitch (server, diff)
	Text::GspeechServer	server
	int	diff

void
gspeech_server_get_languages (server)
	Text::GspeechServer	server
	PPCODE:
	{
		GList * list, *tmp;
		int i;
		list = gspeech_server_get_languages (server);
		for (tmp=list; tmp; tmp= tmp->next) {
			XPUSHs(sv_2mortal(newSVpv(tmp->data, 0)));
		}
		g_list_foreach(list, g_free, NULL);
		g_list_free(list);
	}

void
gspeech_server_get_voices (server, lang)
	Text::GspeechServer	server
	char*	lang
	PPCODE:
	{
		GList * list, *tmp;
		int i;
		list = gspeech_server_get_voices (server, lang);
		for (tmp=list; tmp; tmp= tmp->next) {
			XPUSHs(sv_2mortal(newSVpv(tmp->data, 0)));
		}
		g_list_foreach(list, g_free, NULL);
		g_list_free(list);
	}

char*
description (server)
	Text::GspeechServer	server
	CODE:
	RETVAL = server->description;
	OUTPUT:
	RETVAL

char*
name (server)
	Text::GspeechServer	server
	CODE:
	RETVAL = server->name;
	OUTPUT:
	RETVAL

int
speed (server)
	Text::GspeechServer	server
	CODE:
	RETVAL = server->speed;
	OUTPUT:
	RETVAL

int
pitch (server)
	Text::GspeechServer	server
	CODE:
	RETVAL = server->pitch;
	OUTPUT:
	RETVAL

