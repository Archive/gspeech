# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..1\n"; }
END {print "not ok 1\n" unless $loaded;}
use Text::GspeechServer;
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

$server = new Text::GspeechServer('festival');
$server || die "not ok 2\n";
print "ok 2\n";
#$server->parse("{port=1314;esd;}");
$server->init();
print "Available languages: ", join(', ', $server->get_languages()), "\n";
print "Available voices for english: ", join(', ', $server->get_voices('english')), "\n";
$server->talk("Hello world\n");
$server->set_speed(0);
$server->talk("Now I'm talking very slowly\n");
$server->set_speed(100);
$server->talk("and now a lot faster. I hope you can understand me");
sleep(10);
$server->destroy;

